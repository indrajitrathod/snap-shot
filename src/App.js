import React from "react";
import Search from "./Search";
import { BrowserRouter, Routes, Route } from 'react-router-dom';
import ShowError from "./ShowError";

class App extends React.Component {
  render() {
    return (
      <div>
        <BrowserRouter>
          <Routes>
            <Route path="/" element={<Search />} />
            <Route path="/error" element={<ShowError />} />
          </Routes>
        </BrowserRouter>

      </div>
    );
  };
}

export default App;