import React from "react";
import { Link } from 'react-router-dom';
import Header from "./Header";

class ShowError extends React.Component {

    render() {
        return (
            <>
                {
                    <Link to="/" >
                        <Header />
                    </Link>
                }
                <h1>Something went wrong</h1>
                <h2>Sorry, we can't find the page you are looking for</h2>
                <Link to="/">
                    <div className="error-button-container">
                        <button className="error-button">Go to home</button>
                    </div>
                </Link>
            </>
        );
    };
}

export default ShowError;