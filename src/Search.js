import React from "react";
import Image from "./Image";
import Loader from "./Loader";
import { Navigate } from "react-router-dom";
import Tags from "./Tags";
import Header from "./Header";

class Search extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            search: "",
            invalidSearch: null,
            photos: [],
            noPhotoFound: null,
            searchedImage: null,
            isLoading: false,
            apiError: false
        };

        this.API_KEY = 'af4e5d50cceda3b2ad9b403c2c9ce16e';
    }

    setIsLoading(value) {
        this.setState({
            isLoading: value
        })
    }

    handleInputChange = (event) => {
        this.setState({
            search: event.target.value
        });
    };

    handleTagChange = (event) => {
        this.setState({
            search: event.target.innerText
        }, this.doSearch);
    }

    handleSubmit = (event) => {
        event.preventDefault();

        if (this.state.search.trim().length === 0) {
            this.setState({
                invalidSearch: "Please enter in SearchBox above or choose from below Tags",
                photos: [],
                searchedImage: null,
                isLoading: false
            })
        } else {
            this.doSearch();
        }
    };

    doSearch = () => {
        this.setIsLoading(true);
        fetch(`https://api.flickr.com/services/rest/?method=flickr.photos.search&api_key=${this.API_KEY}&tags=${this.state.search.trim()}&per_page=25&format=json&nojsoncallback=1`)
            .then((response) => {
                if (response.ok) {

                    this.setState({
                        apiError: false
                    });

                    return response.json();
                } else {
                    console.log("Not successful fetch");
                    this.setIsLoading(false);
                    this.setState({
                        apiError: true
                    });
                }
            })
            .then((data) => {

                if (data.photos.photo.length === 0) {
                    this.setState({
                        invalidSearch: null,
                        noPhotoFound: "Images you searched for not found!... please search something else",
                        photos: [],
                        searchedImage: null,
                        isLoading: false
                    });
                } else {
                    let photos = [];
                    photos = data.photos.photo.map((photo) => {
                        let url = `https://live.staticflickr.com/${photo.server}/${photo.id}_${photo.secret}_q.jpg`;

                        return url;
                    });
                    this.setState({
                        invalidSearch: null,
                        noPhotoFound: null,
                        photos: photos,
                        searchedImage: this.state.search + " images"

                    })

                    this.setIsLoading(false);
                }


            })
            .catch((error) => {

                // navigate to error page
                this.setIsLoading(false);
                this.setState({
                    apiError: true
                });
            });

    }

    render() {
        return (
            <React.Fragment>
                <Header />

                {this.state.apiError ?
                    <Navigate to="/error" replace={true} />
                    :
                    null
                }

                <main>
                    <form onSubmit={this.handleSubmit}>
                        <input type="text"
                            placeholder="Search for some meaningful photos"
                            onChange={this.handleInputChange}
                            value={this.state.search}
                        >
                        </input>

                        <button type="submit">
                            <img className="search-icon" src="./search.png" alt='search icon'></img>
                        </button>
                    </form>
                </main>

                <p>{this.state.invalidSearch}</p>

                <Tags tagList={["Mountain", "Beaches", "Birds", "Food"]}
                    tagChange={this.handleTagChange} />

                <h1 className="noPhotoFound">{this.state.noPhotoFound}</h1>

                {this.state.isLoading ?
                    <Loader />
                    :
                    <>
                        <h1 className="searched-image">{this.state.searchedImage}</h1>
                        <div className="image-gallery">
                            {this.state.photos.map((photo) => {
                                return (
                                    <Image key={photo} photo={photo} alt="image" />
                                );
                            })}
                        </div>
                    </>
                }
            </React.Fragment>
        );
    }
}

export default Search;