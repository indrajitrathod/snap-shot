import React from "react";

class Image extends React.Component {

    render() {
        return (
            <div>
                <img src={this.props.photo} alt=''/>
            </div>
        );
    };
}

export default Image;