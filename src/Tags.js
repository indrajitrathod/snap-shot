import React from "react";

class Tags extends React.Component {

    render() {
        return (
            <div className="tags">
                {
                    this.props.tagList.map((tag) => {
                        return (<button key={tag} onClick={this.props.tagChange}>{tag}</button>);
                    })
                }
            </div>
        );
    }
}

export default Tags;